const express = require("express");
const router = express.Router();
const AssistantV2 = require("ibm-watson/assistant/v2");
const { IamAuthenticator } = require('ibm-watson/auth');


const assistant = new AssistantV2({
    version: '2019-02-28',
    authenticator: new IamAuthenticator({
        apikey: process.env.WATSON_ASSISTANT_APIKEY,
    }),
    serviceUrl: process.env.WATSON_ASSISTANT_URL,
});

router.get("/session", async(req, res) => {


    //   assistant.createSession({
    //     assistantId: process.env.WATSON_ASSISTANT_ID,
    //   })
    //     .then(res => {

    //       console.log(JSON.stringify(res.result, null, 2));
    //       let id = JSON.stringify(res.result);
    //       res
    //     })
    //     .catch(err => {
    //       console.log(err);
    //     });

    try {

        const session = await assistant.createSession({
            assistantId: process.env.WATSON_ASSISTANT_ID,
        });
        console.log(res);
        res.json(session['result']);
    } catch (err) {
        res.send('Session creation not Successful');
        console.log(err);
    }

});


router.post("/message", async(req, res) => {


    payload = {
        assistantId: process.env.WATSON_ASSISTANT_ID,
        sessionId: req.headers.session_id,
        input: {
            message_type: "text",
            text: req.body.input,
        },
    };


    try {
        const message = await assistant.message(payload);
        res.json(message["result"]);

    } catch (err) {
        res.send(err);
        console.log(err);
    }

});

module.exports = router;